import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from pages.login import HomePageLogin
from pages.project_page import ProjectPage
from utils.generate_random_text import generate_random_text


@pytest.fixture
def browser():
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()))
    driver.get('http://demo.testarena.pl/zaloguj')
    home_login_page = HomePageLogin(driver)
    home_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    home_login_page.open_admin_panel()
    project = ProjectPage(driver)
    project.wait_for_project_page_load()
    yield driver
    driver.quit()


def test_should_open_admin_panel(browser):
    home_page = HomePageLogin(browser)
    home_page.open_admin_panel()


def test_should_add_new_project_and_search_added_project_by_name(browser):
    random_project_name = generate_random_text(20)
    random_project_prefix = generate_random_text(8)
    random_project_description = generate_random_text(50)
    create_project = ProjectPage(browser)
    create_project.create_new_project(random_project_name, random_project_prefix, random_project_description)
    create_project.open_project_page()
    create_project.search_added_project_by_name(random_project_name)


def test_should_open_project_page(browser):
    project_page = ProjectPage(browser)
    project_page.open_project_page()
