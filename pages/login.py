from selenium.webdriver.common.by import By


class HomePageLogin:

    def __init__(self, browser):
        self.browser = browser

    def login(self, login, password):
        self.browser.find_element(By.CSS_SELECTOR, '#email').send_keys(login)
        self.browser.find_element(By.CSS_SELECTOR, '#password').send_keys(password)
        self.browser.find_element(By.CSS_SELECTOR, '#login').click()

    def open_admin_panel(self):
        self.browser.find_element(By.CSS_SELECTOR, '.header_admin').click()

