import time

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def wait_for_project_page_load(self):
        wait = WebDriverWait(self.browser, 10)
        wait.until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, '.content_title')))

    def create_new_project(self, random_project_name, randon_prefix, random_project_description):
        self.browser.find_element(By.CSS_SELECTOR,
                                  '[href="http://demo.testarena.pl/administration/add_project"]').click()
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(randon_prefix)
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(random_project_description)
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()

    def search_added_project_by_name(self, random_project_name):
        self.browser.find_element(By.CSS_SELECTOR, '#search').send_keys(random_project_name)
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def open_project_page(self):
        # self.browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()
        self.browser.get('http://demo.testarena.pl/administration/projects')
